function ShowElementInReg()
{
	var ch = document.getElementById("id_urlico").checked;
	if(ch)
	{
		document.getElementById('id_RegFamiliya').className = 'preload';
		document.getElementById('id_RegImya').className = 'preload';
		document.getElementById('id_RegOtchestvo').className = 'preload';
		document.getElementById('id_RegName').className = 'preload_';
	}
	else
	{
		document.getElementById('id_RegFamiliya').className = 'preload_';
		document.getElementById('id_RegImya').className = 'preload_';
		document.getElementById('id_RegOtchestvo').className = 'preload_';
		document.getElementById('id_RegName').className = 'preload';
	}
}

function Formdata(data)/*функция проверяет правильность ввода номера телефон 89261234567*/
{
	if(data.value.length > 0 && !(/^[0-9]+z/.test(data.value+"z")))
	{
		alert('Поле "Телефон" содержит недопустимые символы. Пример: 89011234567.');
		return false;
	}

	if(data.value.length > 0 && data.value.length < 11)
	{
		alert('поле "Телефон" должно содержать 11 символов. Пример: 89011234567.');
		return false;
	}
	return true;
}

function CheckText(data)/*функция преобразовывает введенный текст к формату "Формат"*/
{
	var str=data.value;
	var str1=str.charAt(0);
	var str2=str.substring(1);
	str1=str1.toUpperCase();
	str2=str2.toLowerCase();
	str=str1+str2;
	data.value=str;
}

function CheckEmail(data)
{
	if(data.value.length > 0 && !(/^\w+[-_\.]*\w+@\w+-?\w+\.[a-z]{2,4}$/.test(data.value)))
	{
		alert("Введите правильный E-Mail адрес");
		return false;
	}
	return true;
}

function ShowElementInMenu(data) {
    $("#"+data).toggle('fast');
}

function SwitchColor(data) {
    var ch = $('#'+data).css('color');
    var ch2 = $('#'+data).css('background-color');
    if(ch=="rgb(255, 255, 255)") {
        ch="rgb(0, 0, 0)";
    }else{
        ch="rgb(255, 255, 255)";
    }
    if(ch2=="rgb(89, 89, 89)") {
        ch2="rgb(255, 255, 255)";
    }else{
        ch2="rgb(89, 89, 89)";
    }
    $('#'+data).css('color',ch);
    $('#'+data).css('background-color',ch2);
}

