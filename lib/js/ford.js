/**
 * Created with JetBrains PhpStorm.
 * User: ������
 * Date: 19.01.13
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */
// функция постит объект-хэш content в виде формы с нужным action, target
// напр. postToIframe({a:5,b:6}, '/count.php', 'frame1')

function ReInit() {


    $(".ajax").unbind();
    $(".ajax").click(function(){

        var confirm_ = $(this).attr('confirm');
        var elem = $(this).attr('element');
        var href = $(this).attr('href');
        var callback = $(this).attr('callback');

        if (confirm_) if (!confirm(confirm_)) return false;

        showLoader();

        $.ajax({
            url: href,
            type: 'get',
            success: function(data) {
                if (elem) $(''+elem).html(data);
                if (callback) {
                    eval(callback);
                }
                ReInit();
                hideLoader();
            }
        });

        return false;

    });


    $(".ajaxform").unbind();
    $(".ajaxform").submit(function(){
        var action = $(this).attr('action');
        var formData = $(this).serialize();
        var callback = $(this).attr('callback');
        showLoader();
        $.post(action,formData,function(message){
            if (callback) {
                eval(callback);
            }
            ReInit();
            hideLoader();
        });
        return false;
    });
    $(".rowcolorchange").unbind();
    $(".rowcolorchange").click(function(){
        $(this).parent('tr').toggleClass("colorfull");
    });

}


function refreshlist(Id) {
    $(".theList").load("/catalog/do_getCatalog",function(){ ReInit(); $(".knotsid-"+Id).click();});
}

function refreshlistCars() {
    $("#resCars").load("/catalog/do_getCars",function(){ ReInit(); $("buttonCars").click();});
}

function refreshlistSklads() {
//    $(".buttonSklads").click();
    location.reload();
}

function showLoader() {
    $("#loader").css('display','table');
}

function hideLoader() {
    $("#loader").hide();
}

$(document).ready(function(){

    ReInit();

});


    /** OLD SCRIPTS ***/

    function confirmDelete() { /*подтверждение удаления*/
        if (confirm("Вы подтверждаете удаление?")) {
            return true;
        } else {
            return false;
        }
    }

    function confirmContinue() { /*подтверждение удаления*/
        if (confirm("Выхотите продолжить?")) {
            return true;
        } else {
            return false;
        }
    }

    function setFullCookie (name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
    }

    /*function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca;
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }*/

    function removeCookie(name, path) {
        var date = new Date ( );
        date.setTime(date.getTime() - 1);
        document.cookie = name += "=; expires=" + date.toGMTString() + ((path) ? "; path=" + path : "/");
    }

    setFullCookie( "width", $(window).width() , "Mon, 01-Jan-2018 00:00:00 GMT", "/");
    setFullCookie( "height", $(window).height() , "Mon, 01-Jan-2018 00:00:00 GMT", "/");
    var wid = readCookie("width");
    if(!wid) { window.location.href = "/"; }