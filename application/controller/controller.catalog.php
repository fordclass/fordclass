<?php


	class cCatalog {

		public static function types() {
            global $smarty;
			core::useModel('catalog');
			$mUser = new mCatalog;
			$mUser->showTypes();
            $carsMarka = $mUser->getCarsList('Marka');
            $carsModel = $mUser->getCarsList('Model');
            $smarty->assign('carsMarka',$carsMarka);
            $smarty->assign('carsModel',$carsModel);
		}

		public static function do_addItem() { /*добавить главный пункт меню ZERGEY*/
			core::useModel('catalog');
			$mUser = new mCatalog;
			$mUser->addItem();
//            die ();
		}

		public static function do_addSubItem() { /*добавить главный пункт ПОДменю*/
			core::useModel('catalog');
			$mUser = new mCatalog;
			$mUser->addSubItem();
            die ();
		}

        public static function do_redactItem($Id) { /*редактировать пункт меню*/
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->redactItem($Id);
            die ();
        }

        public static function do_deleteItem($Id) { /*удалить пункт меню*/
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->deleteItem($Id);
            die ();
        }

        public static function do_uploadItemFoto($Id) { /*загрузить фотографию ...*/
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->loadItemFoto($Id,$_SERVER['DOCUMENT_ROOT']."/Images/siteImages/");
            die ();
        }

        /**
         * @param $Id
         * @param $numFoto - номер фотографии по счету 1,2,3 или 4
         * изначально предполагается загружать максимум 4 фотграфии для одной позиции товара
         */
        public static function do_uploadGoodFoto($Id,$numFoto) { /*загрузить фотографию ...*/
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->loadGoodFoto($Id,$_SERVER['DOCUMENT_ROOT']."/Images/goodImages/",$numFoto);
            die ();
        }

        /**
         * Достает позиции каталога по их разделу
         * @param $id
         */
        public static function getGoods ($id,$columnSort) {
            global $smarty;
            core::useModel('catalog');
            $mC = new mCatalog();
            $goods = $mC->getGoodsList($id,$columnSort);
            $prNames = $mC->getGoodProizvNames();
            $images = $mC->explodeGoodImages($goods);
            $carsMarka = $mC->getCarsList('Marka');
            $carsModel = $mC->getCarsList('Model');
            $checkedCars = $mC->explodeCars($id);//id - раздел каталога
            $smarty->assign('carsMarka',$carsMarka);
            $smarty->assign('carsModel',$carsModel);
            $smarty->assign('checkedCars',$checkedCars); // массив автомобилей уже отмеченных в БД
            $smarty->assign('Id',$id);//наименование раздела
            $smarty->assign('prNames',$prNames);
            $smarty->assign('goods',$goods);
            $smarty->assign('images',$images);
            $smarty->display("ajax_goodsList.tpl");
            die();
        }

        public static function do_getCatalog () {
            global $smarty;
            core::useModel('catalog');
            $mC = new mCatalog();
            $goods = $mC->showTypes();
            $smarty->assign('goods',$goods);
            $smarty->display("ajax_catalogList.tpl");
            die();
        }

        public static function do_redactGood($id) {
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->redactGood($id);
        }

        public static function do_getCars() {
            global $smarty;
            core::useModel('catalog');
            $mUser = new mCatalog;
            $carsMarka = $mUser->getCarsList('Marka');
            $carsModel = $mUser->getCarsList('Model');
            $smarty->assign('carsMarka',$carsMarka);
            $smarty->assign('carsModel',$carsModel);
            $smarty->display('ajax_carsList.tpl');
            die();
        }

        public static function doRedactCars($id) {
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->redactCars($id);
        }

        /*
         * $type - тип записи
         * value -> marka
         * value -> model
         */
        public static function doAddCar($type) {
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->addCar($type);
        }

        /*
         * функция сохраняет отредактированные списки применяемости товара к автомобилям
         */
        public static function doRedactCheckedCar($id) {
            core::useModel('catalog');
            $mUser = new mCatalog;
            $mUser->RedactCheckedCar($id);
        }

	}


?>