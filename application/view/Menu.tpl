<div style="padding:0px 10px 0px 20px;width:100%;color:#ffffff;font-size: 13px;font-family: Verdana, Arial, Helvetica, sans-serif;">
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='catalog'}#AED9FF{else}#003d79{/if};">
		<i class="icon-th-list {if $MenuItem!='catalog'}icon-white{/if}"></i>
		<a onclick="ShowElementInMenu('catalog');" style="{if $MenuItem=='catalog'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Каталог</a>
	</div>
    <div id="catalog" class="{if $MenuItem=='catalog'}_preload{else}preload{/if}">
		<div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='types'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='types'}icon-white{/if}"></i> <a href="/catalog/types/" style="{if $SubMenuItem=='types'}color:#000000;{else}color:#ffffff;{/if}">Каталог изм.</a></div>
		</div>
    </div>
    <div
        class="menuitem1"
        style="background-color:{if $MenuItem=='settings'}#AED9FF{else}#003d79{/if};">
        <i class="icon-asterisk {if $MenuItem!='settings'}icon-white{/if}"></i>
        <a onclick="ShowElementInMenu('settings');" style="{if $MenuItem=='settings'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Настройки</a>
    </div>
    <div id="settings" class="{if $MenuItem=='settings'}_preload{else}preload{/if}">
        <div>
            <div class="menuitem2" style="background-color:{if $SubMenuItem=='uploading'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='uploading'}icon-white{/if}"></i> <a href="/settings/uploading/" style="{if $SubMenuItem=='uploading'}color:#000000;{else}color:#ffffff;{/if}" class="buttonSklads">Поставщики</a></div>
        </div>
        <div>
            <div class="menuitem2" style="background-color:{if $SubMenuItem=='proizv'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='proizv'}icon-white{/if}"></i> <a href="/settings/proizv/" style="{if $SubMenuItem=='proizv'}color:#000000;{else}color:#ffffff;{/if}" class="buttonSklads">Производители</a></div>
        </div>
    </div>
</div>