{foreach from=$items item=item}
<div onclick="ShowElementInMenu('redactItem{$item.Id}');" style="cursor:pointer;"><i class="icon-pencil"> </i>&nbsp;&nbsp;{$item.Name}</div>

<div id="redactItem{$item.Id}" class="preload">
    <form class="form-inline ajaxform" method="post" action="/catalog/do_redactItem/{$item.Id}" callback="refreshlist();">
        <div class="input-append">
            <input type="text" name="itemName" value="{$item.Name}" style="width:200px;">
            <button class="btn" type="submit">сохранить</button>
        </div>
    </form>
    <form class="form-inline" enctype="multipart/form-data" method="post" action="/catalog/do_uploadItemFoto/{$item.Id}" target="iframe1">
        <input type="text" value="загружен файл {$item.Image}">
        <input type="file" name="filename">
        <button class="btn" type="submit">загрузить</button>
    </form>
    <iframe name='iframe1' class="preload"></iframe>
    <form class="ajaxform" method="post" action="/catalog/do_deleteItem/{$item.Id}" callback="refreshlist();">
        <button class="btn btn-danger" type="submit" onclick="return confirmDelete();">удалить</button>
    </form>
</div>

<div style="margin-left:20px;">
    {foreach from=$item.submenu item=subitem}
        <li>
            <i class="icon-pencil" onclick="ShowElementInMenu('redactSubitem{$subitem.Id}');" style="cursor:pointer;"> </i>&nbsp;&nbsp;
            <div id="redactSubitem{$subitem.Id}" class="preload">
                <form class="form-inline ajaxform" method="post" action="/catalog/do_redactItem/{$subitem.Id}" callback="refreshlist();">
                    <div class="input-append">
                        <input type="text" name="itemName" value="{$subitem.Name}" style="width:200px;">
                        <button class="btn" type="submit">сохранить</button>
                    </div>
                </form>
                <form class="ajaxform" method="post" action="/catalog/do_deleteItem/{$subitem.Id}" callback="refreshlist();">
                    <button class="btn btn-danger" type="submit" onclick="return confirmDelete();">удалить</button>
                </form>
            </div>
            <a href="/catalog/getGoods/{$subitem.Id}/GoodsList.goodName" class="ajax knotsid-{$subitem.Id}" element="#ajaxresult">{$subitem.Name}</a>
        </li>
    {/foreach}
</div>
{/foreach}