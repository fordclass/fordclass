<table border="0px" width="100%" height="100%">
    <tr height="100%" valign="top">
        <td width="300px" bgcolor="#dadada" style="padding: 15px;">
            <div>
                <input type="submit" class="btn btn-primary" value="Добавить узел" onclick="ShowElementInMenu('addItem');">
                <input type="submit" class="btn btn-inverse" value="Добавить пункт меню" onclick="ShowElementInMenu('addSubItem');">
            </div>

            <div id="addItem" class="preload" style="margin: 15px;">
                <form class="form-inline ajaxform" method="post" action="/catalog/do_addItem/" callback="refreshlist();">
                    <div class="input-append">
                        <input type="text" name="nameItem" style="width:200px;">
                        <input type="submit" class="btn btn-success" value="Добавить">
                    </div>
                </form>
            </div>

            <div id="addSubItem" class="preload" style="margin: 15px;>
                <form class="form-inline ajaxform" method="post" action="/catalog/do_addSubItem/" callback="refreshlist();">
                    <select name="idItem">
                    {foreach from=$itm item=tpl}<option value="{$tpl.Id}">{$tpl.Name}</option>{/foreach}
                    </select>
                    <div class="input-append">
                        <input type="text" name="nameSubItem" style="width:200px;">
                        <input type="submit" class="btn btn-success" value="Добавить">
                    </div>
                </form>
            </div>

            <div class='theList' style="margin-top: 15px;">
                {include file="ajax_catalogList.tpl"}
            </div>
            <div>
                {include file="CarsList.tpl"}
            </div>
        </td>
        <td>
            <div id="ajaxresult"></div>
        </td>
    </tr>
</table>