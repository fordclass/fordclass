<div id="resSklads">
<table class="table table-bordered" style="margin: 20px; width: 80%;">
    <tr>
        <td align="center"><b>Название склада</b></td>
        <td align="center"><b>Обновление</b></td>
    </tr>

    {foreach from=$sklad item=skl}
        <tr onclick="ShowElementInMenu('ss{$skl.Id}');">
            <td>{$skl.sklName}</td>
            <td>{$skl.updateDate}</td>
        </tr>
        <tr id="ss{$skl.Id}" class="preload">
            <td colspan="2">
                <table>
                    <form method="post" action="/settings/uploading/{$skl.Id}" class="ajaxform" callback="refreshlistSklads();">
                        <tr><td colspan="2"><input type="submit" value="Сохранить" class="btn btn-success" style="width: 100%;"></td></tr>
                        <tr bgcolor="grey"><td colspan="2" align="center"><b>Общие данные склада</b></td></tr>
                        <tr>
                            <td width="50%">Название склада / псевдоним склада</td>
                            <td>
                                <input type="text" name="sklName" value="{$skl.sklName}" style="width:168px;">
                                <input type="text" name="sklProName" value="{$skl.sklProName}" style="width:168px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Наценки (10категорий:1-розница)</td>
                            <td>
                                <input type="text" name="Nac1" value="{$skl.Nac1}" style="width:30px;">
                                <input type="text" name="Nac2" value="{$skl.Nac2}" style="width:30px;">
                                <input type="text" name="Nac3" value="{$skl.Nac3}" style="width:30px;">
                                <input type="text" name="Nac4" value="{$skl.Nac4}" style="width:30px;">
                                <input type="text" name="Nac5" value="{$skl.Nac5}" style="width:30px;">
                                <input type="text" name="Nac6" value="{$skl.Nac6}" style="width:30px;">
                                <input type="text" name="Nac7" value="{$skl.Nac7}" style="width:30px;">
                                <input type="text" name="Nac8" value="{$skl.Nac8}" style="width:30px;">
                                <input type="text" name="Nac9" value="{$skl.Nac9}" style="width:30px;">
                                <input type="text" name="Nac10" value="{$skl.Nac10}" style="width:30px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Доп инфо о складе</td>
                            <td>
                                <input type="text" name="sklInfo" value="{$skl.sklInfo}" style="width:357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Срок поставки</td>
                            <td>
                                <div class="input-prepend input-append">
                                    <span class="add-on" style="background-color: rgba(255,0,0,0.50);">от (дней)</span>
                                    <input type="number" style="width: 100px;" name="dostSrokMin" value="{$skl.dostSrokMin}">
                                    <input type="number" style="width: 100px;" name="dostSrokMax" value="{$skl.dostSrokMax}">
                                    <span class="add-on" style="background-color: #32cd32;">до (дней)</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Скидка от базовой цены<br>формирование закупочной цены</td>
                            <td>
                                <input type="text" name="priceSkidka" value="{$skl.priceSkidka}" style="width:357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Валюта (для складов с ценами, если валюта не выбрана, значит это таблица без цен)</td>
                            <td>
                                <select style="width:357px;">
                                    <option value="" {if $skl.priceVal eq NULL}checked{/if}>валюта не выбрана</option>
                                    <option value="RUR" {if $skl.priceVal eq "RUR"}checked{/if}>RUR</option>
                                    <option value="EUR" {if $skl.priceVal eq "EUR"}checked{/if}>EUR</option>
                                    <option value="USD" {if $skl.priceVal eq "USD"}checked{/if}>USD</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Предоплата</td>
                            <td>
                                <input type="checkbox" name="prePay" value="{$skl.prePay}">
                            </td>
                        </tr>
                        <tr bgcolor="grey"><td colspan="2" align="center"><b>Отправка заказов</b></td></tr>
                        <tr>
                            <td>Автоматическая отправка заказов</td>
                            <td>
                                <input type="checkbox" name="AutoSender" value="1" {if $skl.AutoSender eq 1}checked{/if}>
                            </td>
                        </tr>
                        <tr>
                            <td>Текст письма</td>
                            <td>
                                <textarea name="mailTextZakaz" style="width: 357px; height: 100px;">{$skl.mailTextZakaz}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>Почтовые ящики для отправки заказов (max 3шт)</td>
                            <td>
                                <input type="text" name="mailForSend" value="{$skl.mailForSend}" style="width:357px;">
                            </td>
                        </tr>
                        <tr bgcolor="grey"><td colspan="2" align="center"><b>Способ получения файлов</b></td></tr>
                        <tr>
                            <td>Получать файл:</td>
                            <td>
                                <input type="radio" name="fileGetType" value="mail" {if $skl.fileGetType eq "mail"}checked{/if}> на почту
                                <input type="radio" name="fileGetType" value="link" {if $skl.fileGetType eq "link"}checked{/if}> скачивать по ссылке
                            </td>
                        </tr>
                        <tr>
                            <td>Имя файла АРХИВА (если прайсы заархивированы)</td>
                            <td>
                                <input type="text" name="fileArchiveName" value="{$skl.fileArchiveName}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Имя файла (либо в письме, либо в архиве)</td>
                            <td>
                                <input type="text" name="fileName" value="{$skl.fileName}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Имя файла для переименования и сохранения</td>
                            <td>
                                <input type="text" name="fileReName" value="{$skl.fileReName}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Номер строки, с которой программа начинает читать файл</td>
                            <td>
                                <input type="text" name="fileReadLine" value="{$skl.fileReadLine}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>От кого приходят письма (Иван Иванович mail@mail.ru - можно указывать почту или подпись или часть из этих строк, но не все сразу)</td>
                            <td>
                                <input type="text" name="mailFrom" value="{$skl.mailFrom}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Тема письма</td>
                            <td>
                                <input type="text" name="mailPost" value="{$skl.mailPost}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Дни недели, по которым скачиваются файлы по ссылке</td>
                            <td>
                                <input type="text" name="loadDays" value="{$skl.loadDays}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Ссылка, по которой скачивается файл</td>
                            <td>
                                <input type="text" name="loadLink" value="{$skl.loadLink}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr bgcolor="grey"><td colspan="2" align="center"><b>Стоимость доставки (для некоторых складов)</b></td></tr>
                        <tr>
                            <td>Стоимость доставки за 1кг веса брутто</td>
                            <td>
                                <input type="text" name="dostPriceZak" value="{$skl.dostPriceZak}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Валюта цены доставки</td>
                            <td>
                                <select style="width:357px;">
                                    <option value="" {if $skl.dostValuta eq NULL}checked{/if}>валюта не выбрана</option>
                                    <option value="RUR" {if $skl.dostValuta eq "RUR"}checked{/if}>RUR</option>
                                    <option value="EUR" {if $skl.dostValuta eq "EUR"}checked{/if}>EUR</option>
                                    <option value="USD" {if $skl.dostValuta eq "USD"}checked{/if}>USD</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Стоимость доставки за 1кг веса брутто ДЛЯ КЛИЕНТА (в валюте)</td>
                            <td>
                                <input type="text" name="dostPrice" value="{$skl.dostPrice}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr bgcolor="grey"><td colspan="2" align="center"><b>Склад в БД</b></td></tr>
                        <tr>
                            <td>Тип таблицы для склада</td>
                            <td>
                                <input type="hidden" name="preSklType" value="{$skl.sklType}">
                                <label><input type="radio" name="sklType" value="1" {if $skl.sklType==1}checked{/if}>[1]Наличие и цены</label>
                                <label><input type="radio" name="sklType" value="2" {if $skl.sklType==2}checked{/if}>[2]Наличие без цен, цены в отдельной таблице</label>
                                <label><input type="radio" name="sklType" value="3" {if $skl.sklType==3}checked{/if}>[3]Цены</label>
                            </td>
                        </tr>
                        <tr>
                            <td>Таблица уже сещуствует (в настройки не сохраняется))</td>
                            <td>
                                <input type="checkbox" name="table" value="1">
                            </td>
                        </tr>
                        <tr>
                            <td>Наименование таблицы (тип таблицы 1 или 2)<br>Если поле было пустое, при заполнении и сохранении создается таблица, при удалении данных из поля, т.е. поле равно NULL, таблица удаляется.</td>
                            <td>
                                <input type="hidden" name="preTableName" value="{$skl.tableName}">
                                <input type="text" name="tableName" value="{$skl.tableName}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Наименование таблицы (тип таблицы 3)<br>в этом случаем таблица не создается и не удаляется, это поле работает как ссылка на таблицу, где брать цены</td>
                            <td>
                                <input type="text" name="tableNamePrice" value="{$skl.tableNamePrice}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Соответствие колонок таблицы колонкам загружаемого файла<br>(1=Art,	2=Kod, 3=Name, 4=Proizv, 5=Price, 6=Ost)<br>например: //1//2//3,4//5//6//7//</td>
                            <td>
                                <input type="text" name="fileParsingIfo" value="{$skl.fileParsingIfo}" style="width: 357px;">
                            </td>
                        </tr>
                        <tr bgcolor="grey"><td colspan="2" align="center"><b>Блокировка склада</b></td></tr>
                        <tr>
                            <td>Блокировка (не показывать в результатах поиска)</td>
                            <td>
                                <input type="checkbox" name="Block" value="1" {if $skl.Block eq 1}checked{/if}>
                            </td>
                        </tr>
                        <tr><td colspan="2"><input type="submit" value="Сохранить" class="btn btn-success" style="width: 100%;"></td></tr>
                    </form>
                        <tr><td colspan="2" align="right"><a href="/settings/doDeleteSklad/{$skl.Id}" class="btn btn-danger ajax" confirm='Удалить?' callback="refreshlistSklads();">Удалить</a></td></tr>
                </table>
            </td>
        </tr>
    {/foreach}
</table>
</div>
<div style="margin: 20px;">
    <form method="post" action="/settings/uploading/new">
        <div class="input-append">
            <input type="text" name="newSkladName" value="Название склада" style="width: 200px;" onfocus="if(this.value=='Название склада')this.value='';" onblur="if(this.value=='')this.value='Название склада';"><input type="submit" value="Добавить новый склад" class="btn btn-success" onclick="return confirmContinue();">
        </div>
    </form>
</div>