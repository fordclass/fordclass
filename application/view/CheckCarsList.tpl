<div>
    {counter start=0 skip=1}
    <form class="form-inline ajaxform" method="post" action="/catalog/doRedactCheckedCar/{$item.goodId}" target="_blank">
    {foreach from=$carsMarka item=marka}
        <h2>{$marka.Name}</h2>
        <ul style="list-style: none; margin:0; padding: 0;">
            {foreach from=$carsModel[$marka.Name] item=model}
                <li style="list-style: none; margin:0; padding: 0; display: inline-block; width:200px;" align="left">
                    <input type="checkbox" name="checkbox[{counter}]" value="{$model.Id}"
                        {foreach from=$checkedCars[$item.goodId] item=car}
                            {if $car==$model.Id}checked{/if}
                        {/foreach}
                    > {$model.Name} {$model.Description}
                </li>
            {/foreach}
        </ul>
    {/foreach}
    <input type="submit" value="Сохранить" class="btn btn-success">
    </form>
</div>