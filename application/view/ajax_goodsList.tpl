Раздел {$Id}

<table class="table table-bordered table-hover table-condensed" style="margin: 15px;width: 97%;">
    <tr>
        <td align="center">Артикул</td>
        <td align="center">Произ-ль</td>
        <td align="center">Цена зак. в валюте</td>
        <td align="center">Цена розн. руб.</td>
        <td align="center">Наименование</td>
        <td align="center">Применимость</td>
    </tr>
{foreach from=$goods item=item}

    <tr id="idtr{$item.goodId}" ondblclick="ShowElementInMenu('lineGood{$item.goodId}');SwitchColor('idtr{$item.goodId}');">
        <td class="rowcolorchange">{$item.goodArt}</td>
        <td class="rowcolorchange">{$item.prName}</td>
        <td class="rowcolorchange">{$item.goodPriceValZak} {$item.Valuta}</td>
        <td class="rowcolorchange">{$item.goodPrice}</td>
        <td class="rowcolorchange">{$item.goodName}</td>
        <td class="rowcolorchange">{$item.goodPrimenimost}</td>
    </tr>
    <form class="ajaxform" method="post" action="/catalog/do_redactGood/{$item.goodId}" callback="refreshlist({$Id});">
    <tr id="lineGood{$item.goodId}" class="preload" style="background-color: #f5f5f5;">
        <td colspan="6">
            <table class="table" style="background-color: #f5f5f5;">
                <tr>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Art" value="{$item.goodArt}" style="width: 150px;" onclick="this.style.width='300px';" onblur="this.style.width='150px';">
                            <button class="btn" type="button">Артикул</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <select name="idProizv" style="width: 164px;">
                                {foreach from=$prNames item=pr}
                                    <option value="{$pr.prId}" {if $pr.prId eq $item.goodIdProizv} selected{/if}>{$pr.prName}</option>
                                {/foreach}
                            </select>
                            <button class="btn" type="button">Производитель</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <select style="width: 150px;" name="TypeGood">
                                <option value="shlang">шланг,трубка,патрубок</option>
                                <option value="kuzov">деталь кузова (металл)</option>
                                <option value="optika">оптика:фары,фонари,поворотники,птф</option>
                                <option value="podveska">деталь ходовой части</option>
                                <option value="krepegh">крепеж:клипса,кронштейн,болт,гайка</option>
                                <option value="engine">деталь двигателя</option>
                                <option value="electr">деталь электрики</option>
                                <option value="zaghiganie">деталь системы зажигания</option>
                                <option value="nasosmeh">насос мех:помпа,ебнзонасос мех.</option>
                                <option value="nasoselectr">насос электр:бензонасос,моторч.омыв.</option>
                                <option value="motorelectr">мотор электр:стеклопод,трапец.с/о</option>
                                <option value="sensor">датчик,клапан</option>
                            </select>
                            <button class="btn" type="button">Транскрипция</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Kod" value="{$item.goodKod}" style="width: 150px;" onclick="this.style.width='300px';" onblur="this.style.width='150px';">
                            <button class="btn" type="button">Кросы ориг</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Kod2" value="{$item.goodKod2}" style="width: 150px;" onclick="this.style.width='300px';" onblur="this.style.width='150px';">
                            <button class="btn" type="button">Кросы НЕориг</button>
                        </div>
                    </td>
                    <td>
                        <label class="checkbox">
                            <input type="checkbox" name="ShowInPrice" {if $item.goodShowInPrice} checked{/if}> Показывать в прайс листе
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Name" value="{$item.goodName}" style="width: 150px;" onclick="this.style.width='300px';" onblur="this.style.width='150px';">
                            <button class="btn" type="button">Наименование</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Primenimost" value="{$item.goodPrimenimost}" style="width: 150px;" onclick="this.style.width='300px';" onblur="this.style.width='150px';">
                            <button class="btn" type="button">Применимость</button>
                        </div>
                    </td>
                    <td>
                        <label class="checkbox">
                            <input type="checkbox" name="ShowInShop" {if $item.goodShowInShop} checked{/if}> Показывать в интернет магазине
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input-append">
                            <input type="text" name="PriceValZak" value="{$item.goodPriceValZak}" style="width: 90px;">
                            <select name="Valuta" style="width: 60px;">
                                <option value="USD" {if $item.goodValuta eq 'USD'} selected{/if}>USD</option>
                                <option value="EUR" {if $item.goodValuta eq 'EUR'} selected{/if}>EUR</option>
                                <option value="RUR" {if $item.goodValuta eq 'RUR'} selected{/if}>RUR</option>
                            </select>
                            <button class="btn" type="button">Цена прих. УЕ</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <input type="text" name="PriceVal" value="{$item.goodPriceVal}" style="width: 90px;">
                            <input type="text" name="" value="{$item.goodPriceVal}" style="width: 46px;">
                            <button class="btn" type="button">Цена УЕ</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-prepend input-append">
                            <span class="add-on" style="background-color: rgba(255,0,0,0.50);">min кол-во</span><input type="number" style="width: 100px;" name="MinKol" value="{$item.goodMinKol}"><input type="number" style="width: 100px;" name="MaxKol" value="{$item.goodMaxKol}"><span class="add-on" style="background-color: #32cd32;">max кол-вл</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Price" value="{$item.goodPrice}" style="width: 90px;">
                            <input type="text" name="" value="RUR" style="width: 45px;">
                            <button class="btn" type="button">Цена RUR</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <input type="text" name="StopPrice" value="{$item.goodStopPrice}" style="width: 90px;">
                            <input type="text" name="" value="RUR" style="width: 46px;">
                            <button class="btn" type="button">СТОП Цена</button>
                        </div>
                    </td>
                    <td>
                        <div class="input-append">
                            <input type="text" name="Skidka" value="{$item.goodSkidka}" style="width: 150px;">
                            <button class="btn" type="button">Скидка на данный товар, %</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Наценки (10 категорий, 1-розница)</td>
                    <td colspan="2">
                        <input type="text" name="goodNac1" value="{$item.goodNac1}" style="width:30px;">
                        <input type="text" name="goodNac2" value="{$item.goodNac2}" style="width:30px;">
                        <input type="text" name="goodNac3" value="{$item.goodNac3}" style="width:30px;">
                        <input type="text" name="goodNac4" value="{$item.goodNac4}" style="width:30px;">
                        <input type="text" name="goodNac5" value="{$item.goodNac5}" style="width:30px;">
                        <input type="text" name="goodNac6" value="{$item.goodNac6}" style="width:30px;">
                        <input type="text" name="goodNac7" value="{$item.goodNac7}" style="width:30px;">
                        <input type="text" name="goodNac8" value="{$item.goodNac8}" style="width:30px;">
                        <input type="text" name="goodNac9" value="{$item.goodNac9}" style="width:30px;">
                        <input type="text" name="goodNac10" value="{$item.goodNac10}" style="width:30px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <input type="submit" class="btn btn-success" value="Сохранить"></form>
                    </td>
                    <td></td>
                    <td align="right" colspan="1">
                        <button type="submit" class="btn btn-warning" style="width: 300px;" onclick="ShowElementInMenu('sl{$item.goodArt}');"><i class="icon-align-justify"> </i> Применяемость</button>
                    </td>
                </tr>
            </table>

<div id="sl{$item.goodArt}" style="position: absolute; z-index:10; background-color: rgba(40,40,40,0.83); width: 100%; height: 100%; left: 0px; top: 0px;" class="preload" align="center">
    <a onclick="ShowElementInMenu('sl{$item.goodArt}');" style="font-size: 40px; color: #ffffff;">закрыть</a>
    <div style="position: absolute; z-index:11; background-color: rgba(234,234,234,1); width: 70%; height: 80%; left: {$smarty.cookies.width*0.15}px; top: {$smarty.cookies.height*0.1}px; -webkit-border-radius:3px;border-radius:3px;">
        {include file="CheckCarsList.tpl"}
    </div>
</div>

            <div style="background-color: #f5f5f5;">
                <ul class="thumbnails">
                    <li class="span4">
                        {if $images[$item.goodId][0][1]}
                            <div class="thumbnail">
                                <img src="/Images/goodImages/smallGoodImages/{$images[$item.goodId][0][1]}" alt="">
                            </div>
                        {else}
                            <form class="form-inline" enctype="multipart/form-data" method="post" action="/catalog/do_uploadGoodFoto/{$item.goodId}/1" target="iframe{$item.goodId}1">
                                <input type="file" name="filename">
                                <button class="btn" type="submit">загрузить 1</button>
                            </form>
                            <iframe name="iframe{$item.goodId}1" class="preload"></iframe>
                        {/if}
                    </li>
                    <li class="span4">
                        {if $images[$item.goodId][0][2]}
                            <div class="thumbnail">
                                <img src="/Images/goodImages/smallGoodImages/{$images[$item.goodId][0][2]}" alt="">
                            </div>
                            {else}
                            <form class="form-inline" enctype="multipart/form-data" method="post" action="/catalog/do_uploadGoodFoto/{$item.goodId}/2" target="iframe{$item.goodId}2">
                                <input type="file" name="filename">
                                <button class="btn" type="submit">загрузить 2</button>
                            </form>
                            <iframe name="iframe{$item.goodId}2" class="preload"></iframe>
                        {/if}
                    </li>
                    <li class="span4">
                        {if $images[$item.goodId][0][3]}
                            <div class="thumbnail">
                                <img src="/Images/goodImages/smallGoodImages/{$images[$item.goodId][0][3]}" alt="">
                            </div>
                            {else}
                            <form class="form-inline" enctype="multipart/form-data" method="post" action="/catalog/do_uploadGoodFoto/{$item.goodId}/3" target="iframe{$item.goodId}3">
                                <input type="file" name="filename">
                                <button class="btn" type="submit">загрузить 3</button>
                            </form>
                            <iframe name="iframe{$item.goodId}3" class="preload"></iframe>
                        {/if}
                    </li>
                    <li class="span4">
                        {if $images[$item.goodId][0][4]}
                            <div class="thumbnail">
                                <img src="/Images/goodImages/smallGoodImages/{$images[$item.goodId][0][4]}" alt="">
                            </div>
                            {else}
                            <form class="form-inline" enctype="multipart/form-data" method="post" action="/catalog/do_uploadGoodFoto/{$item.goodId}/4" target="iframe{$item.goodId}4">
                                <input type="file" name="filename">
                                <button class="btn" type="submit">загрузить 4</button>
                            </form>
                            <iframe name="iframe{$item.goodId}4" class="preload"></iframe>
                        {/if}
                    </li>
                </ul>
            </div>
        </td>
    </tr>
{/foreach}
</table>