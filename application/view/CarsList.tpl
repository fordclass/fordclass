<div style="padding-top: 30px;">
    <button class="btn btn-success" onclick="ShowElementInMenu('Cars');" style="width: 280px;">Показать автомобили</button>
    <div id="Cars" class="preload">
        <div id="resCars">
            <a href="/catalog/do_getCars" class="btn btn-info ajax buttonCars" element="#ajaxresultCarsList" style="margin-top:15px;margin-bottom:15px; width:210px;">Список всех авто</a>
            <div id="ajaxresultCarsList"></div>
        </div>
        <input type="submit" class="btn btn-inverse" value="Добавить марку" onclick="ShowElementInMenu('addMarka');" style="width: 280px;">
        <div id="addMarka" class="preload" style="padding: 10px;">
            <form class="form-inline ajaxform" method="post" action="/catalog/doAddCar/marka" callback="refreshlist();">
                <div class="input-append">
                    <input type="text" name="Name" style="width: 120px;">
                    <input type="submit" class="btn btn-success" value="Добавить">
                </div>
            </form>
        </div>
        <input type="submit" class="btn btn-success" value="Добавить модель" onclick="ShowElementInMenu('addModel');" style="width: 280px;">
        <div id="addModel" class="preload" style="padding: 10px;">
            <form class="form-inline ajaxform" method="post" action="/catalog/doAddCar/model" callback="refreshlist();">
                <select name="SubId" style="width: 216px;">
                    {foreach from=$carsMarka item=marka}<option value="{$marka.Id}">{$marka.Name}</option>{/foreach}
                </select>
                <input type="text" name="Name" value="" style="width: 210px;" onFocus="if (this.value=='модель') this.value='';" onBlur="if (this.value=='') this.value='модель';">
                <div class="input-append">
                    <input type="text" name="Year" style="width: 120px;">
                    <input type="submit" class="btn btn-success" value="Добавить">
                </div>
            </form>
        </div>
    </div>
</div>