<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="/lib/mycss.css">
    <link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/lib/bootstrap/css/bootstrap-responsive.css">

    <title></title>

    <meta name="keywords" content="">
    <meta name= "description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!--Для корректного отображения и работы динамических медиа-запросов, необходимо включить viewport meta - тег.-->

	<meta http-equiv="expires" content="Tue, 30 January 2011 12:00:00 GMT">

	{literal}
        <script type="text/javascript" src="/lib/js/jquery-1.9.0.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/lib/functions.js"></script>
        <script type="text/javascript" src="/lib/js/ford.js"></script>
	{/literal}


</head>
<body bgcolor="#003d79">

{if $smarty.session.Sotrudnik}

<table border="0" cellpadding="0" cellspacing="0" width="{$smarty.cookies.width}px" height="{$smarty.cookies.height}px">
	<tr>
		<td width="200px" align="left" valign="top">
			{include file="disAutorisation.tpl"}
            {include file="formPoisk.tpl"}
			{include file="Menu.tpl"}
			{foreach from=$templates[1] item=tpl}{include file="`$tpl`.tpl"}{/foreach}
		</td>
		<td width="{$smarty.cookies.width-200}px" align="left" valign="top" style="background:#ffffff;">
			{foreach from=$templates[2] item=tpl}{include file="`$tpl`.tpl"}{/foreach}
		</td>
	</tr>
</table>

{else}

	{include file="Autorisation.tpl"}

{/if}

<div id='loader'><div>LOADING</div></div>

</body>
</html>