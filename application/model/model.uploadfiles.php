<?php


class mUploadfiles {

    function ImportFileNew() {
        global $base;
        $Sklad = $base->select_("SELECT * FROM `Sklads`;");

        $dir = opendir($_SERVER['DOCUMENT_ROOT']."/Files/"); // Открываем директорию
        while(($file = readdir($dir))) { // В цикле считываем её содержимое
            if($file!='.' && $file!='..') { // Если текущий объект является файлом - выводим его
                $FileForLoad='0';
                //проверяем, есть ли этот файл в списке загрузок, если нет, удаляем его
                //если нашли файл из списка, прекращаем цикл и работаем дальше с этим файлом
                $i=0;
                foreach($Sklad as $skl) {
                    if($skl['fileReName']==$file) {
                        $FileForLoad=1;
                        $IPos=$i;
                    }
                    $i++;
                }
                if($FileForLoad==0) { //в случае несовпадения имен файлов со списком, удаляем файл с сервера
                    unlink($_SERVER['DOCUMENT_ROOT']."/Files/".$file);
                }
            }
        }
        list($column[1],$column[2],$column[3],$column[4],$column[5],$column[6])=explode("//",$Sklad[$IPos]['fileParsingInfo']);

        $row = $base->do_("DELETE FROM `".$Sklad[$IPos]['tableName']."`;"); //перед загрузкой очищаем таблицу
        for($j=1;$j<=6;$j++){ //разбиваем строку fileParsingInfo и создаем массив, который будет использован далле в цикле
            $str=null;
            $k=0;
            for($i=0;$i<strlen($column[$j]);$i++)
            {
                if($column[$j][$i]!="0" && $column[$j][$i]!="1" && $column[$j][$i]!="2" && $column[$j][$i]!="3" && $column[$j][$i]!="4" && $column[$j][$i]!="5" && $column[$j][$i]!="6" && $column[$j][$i]!="7" && $column[$j][$i]!="8" && $column[$j][$i]!="9"){//если символ разделитель
                    $iRow[$j][$k]=$str;
                    $str=null;
                    $iRazdelitel[$j][$k]=$column[$j][$i];
                    $k++;
                }else{
                    $str=$str.$column[$j][$i];
                }
                if(empty($column[$j][$i+1])){ //дополнительное условие, если следующий символ пустой, значит конец строки - записываем значение в $iRow[$k]
                    $iRow[$j][$k]=$str;
                    $str=null;
                    $iRazdelitel[$j][$k]=NULL;
                    $k++;
                }
            }
        }

        if(substr_count($Sklad[$IPos]['fileReName'],'xls')>0){ //если файл с расширением xls то используем excel_reader
            require_once ($_SERVER['DOCUMENT_ROOT']."/lib/excel_reader.php");
            $data = new Spreadsheet_Excel_Reader($_SERVER['DOCUMENT_ROOT']."/Files/".$Sklad[$IPos]['fileReName'],true,"UTF-8");
            $data->setUTFEncoder('mb');
            for($k=0;$k<=20;$k++) {
                for($i=1;$i<=$data->sheets[$k]['numRows'];$i++) {
                    $SqlStr="INSERT INTO `".$Sklad[$IPos]['tableName']."` (`Art`,`Kod`,`Name`,`Proizv`,`Price`,`Ost`) VALUES('";

                    for($n=0;$n<4;$n++) {
                        $Art=$data->val($i,$iRow[1][$n],0);
                        $Art=trim($Art);
                        $Art = preg_replace("/[^a-zA-Z0-9]/",'',$Art);
                        if(!empty($iRow[1][$n]) || !empty($iRazdelitel[1][$n]))$SqlStr.=self::icnv($Art.$iRazdelitel[1][$n],"UTF8");
                    }
                    $SqlStr.="','";

                    for($n=0;$n<4;$n++) {
                        $Kod=$data->val($i,$iRow[2][$n],0);
                        $Kod=trim($Kod);
                        $Kod=str_replace(";","/",$Kod);
                        $Kod=str_replace("&","/",$Kod);
                        $Kod = preg_replace("/[^a-zA-Z0-9\/]/",'',$Kod);
                        if(!empty($iRow[2][$n]) || !empty($iRazdelitel[2][$n]))$SqlStr.=self::icnv($Kod.$iRazdelitel[2][$n],"UTF8");
                    }
                    $SqlStr.="','";

                    for($n=0;$n<4;$n++) {
                        $Name=$data->val($i,$iRow[3][$n],0);
                        $Name=str_replace("'","",$Name);
                        if(!empty($iRow[3][$n]) || !empty($iRazdelitel[3][$n])) {
                            $SqlStr.=$Name.$iRazdelitel[3][$n];
                        }
                    }
                    $SqlStr.="','";
                    for($n=0;$n<4;$n++){if(!empty($iRow[4][$n]) || !empty($iRazdelitel[4][$n]))$SqlStr.=self::icnv($data->sheets[0]['cells'][$i][$iRow[4][$n]].$iRazdelitel[4][$n],"UTF8");}$SqlStr.="','";

                    for($n=0;$n<4;$n++) {
                        if(!empty($iRow[5][$n]) || !empty($iRazdelitel[5][$n])) {
                            $gg=$data->raw($i,$iRow[5][$n],0);
                            if(!empty($gg)) {
                                $Price=$data->raw($i,$iRow[5][$n],0);
                            } else {
                                $Price=$data->sheets[0]['cells'][$i][$iRow[5][$n]];
                            }
                            if(substr_count($Price,",")>0 && substr_count($Price,".")>0)$Price=str_replace(",","",$Price);
                            $Price=str_replace(",",".",$Price);
                            $Price=str_replace(" ","",$Price);
                            $Price=str_replace("RUB","",$Price);
                            $SqlStr.=self::icnv($Price.$iRazdelitel[5][$n],"UTF8");
                        }
                    }

                    $SqlStr.="','";
                    for($n=0;$n<4;$n++){if(!empty($iRow[6][$n]) || !empty($iRazdelitel[6][$n]))$SqlStr.=self::icnv($data->raw($i,$iRow[6][$n],0).$iRazdelitel[6][$n],"UTF8");}$SqlStr.="');";
                    $sql=$base->do_($SqlStr);
                }
            }
        }
        if(substr_count($Sklad[$IPos]['fileReName'],'csv')>0 || substr_count($Sklad[$IPos]['fileReName'],'txt')>0) {
            $f=fopen($_SERVER['DOCUMENT_ROOT']."/Files/".$Sklad[$IPos]['fileReName'],"r");
            while($data=fgets($f))
            {
                list($Pole[1],$Pole[2],$Pole[3],$Pole[4],$Pole[5],$Pole[6],$Pole[7],$Pole[8],$Pole[9],$Pole[10],$Pole[11],$Pole[12],$Pole[13],$Pole[14],$Pole[15],$Pole[16],$Pole[17],$Pole[18],$Pole[19],$Pole[20])=explode(";",$data);
                $SqlStr="INSERT INTO `".$Sklad[$IPos]['tableName']."` (`Art`,`Kod`,`Name`,`Proizv`,`Price`,`Ost`) VALUES('";
                //art
                for($n=0;$n<4;$n++)
                {
                    $Art=$Pole[$iRow[1][$n]];
                    $Art=trim($Art);
                    $Art = preg_replace("/[^a-zA-Z0-9]/",'',$Art);
                    if(!empty($iRow[1][$n]) || !empty($iRazdelitel[1][$n]))$SqlStr.=$Art.$iRazdelitel[1][$n];
                }
                $SqlStr.="','/";
                //kod
                for($n=0;$n<4;$n++) {
                    $Kod=$Pole[$iRow[2][$n]];
                    $Kod=trim($Kod);
                    $Kod=str_replace(";","/",$Kod);
                    $Kod=str_replace("&","/",$Kod);
                    $Kod=str_replace(",","/",$Kod);
                    $Kod = preg_replace("/[^a-zA-Z0-9\/]/",'',$Kod);
                    if(!empty($iRow[2][$n]) || !empty($iRazdelitel[2][$n]))$SqlStr.=$Kod.$iRazdelitel[2][$n];
                }
                $SqlStr.="/','";
                //name
                for($n=0;$n<4;$n++){if(!empty($iRow[3][$n]) || !empty($iRazdelitel[3][$n]))$SqlStr.=$Pole[$iRow[3][$n]].$iRazdelitel[3][$n];}$SqlStr.="','";
                //proizv
                for($n=0;$n<4;$n++){if(!empty($iRow[4][$n]) || !empty($iRazdelitel[4][$n]))$SqlStr.=$Pole[$iRow[4][$n]].$iRazdelitel[4][$n];}$SqlStr.="','";
                //price
                for($n=0;$n<4;$n++) {
                    if(!empty($iRow[5][$n]) || !empty($iRazdelitel[5][$n])) {
                        $Price=$Pole[$iRow[5][$n]];
                        if(substr_count($Price,",")>0 && substr_count($Price,".")>0)$Price=str_replace(",","",$Price);
                        $Price=str_replace(",",".",$Price);
                        $Price=str_replace(" ","",$Price);
                        $SqlStr.=$Price.$iRazdelitel[5][$n];
                    }
                }
                $SqlStr.="','";
                //ost
                for($n=0;$n<4;$n++){if(!empty($iRow[6][$n]) || !empty($iRazdelitel[6][$n]))$SqlStr.=$Pole[$iRow[6][$n]].$iRazdelitel[6][$n];}$SqlStr.="');";
                $sql=$base->do_($SqlStr);
            }
            fclose($f);
        }
        if($IPos>=0) unlink($_SERVER['DOCUMENT_ROOT']."/Files/".$Sklad[$IPos]['fileReName']); //удаляем файл
        $sql = $base->do_("UPDATE `Sklads` SET `updateDate`='".date('d.m.y H:i')."' WHERE `Id`='".$Sklad[$IPos]['Id']."';");
    }


    function GetFileFromMail() {
        global $base;

        $Sklad = $base->select_("SELECT * FROM `Sklads` WHERE `Block`='0' AND `fileGetType`='mail';");

        define( 'MAILBOX', '{pop.mail.ru:110/pop3}INBOX' );
        define( 'USER', 'test-code@mail.ru' );
        define( 'PASS', 'atljcttd' );

        // Возможные виды кодирования данных. Ввод этих констант нужен только для дальнейшего улучшения читабельности кода.
        define( 'ENC_7BIT', 0 );
        define( 'ENC_8BIT', 1 );
        define( 'ENC_BINARY', 2 );
        define( 'ENC_BASE64', 3 ); // Чаще всего для передачи вложений их кодируют в base64
        define( 'ENC_QUOTED_PRINTABLE', 4 );
        define( 'ENC_OTHER', 5 );

        if(!function_exists('imap_open'))die('IMAP lib does not exist');// Если IMAP-библиотека не подключена или отсутствует в этой сборке
        $attachment = array(); // Массив для attachment'ов (вложений)
        if(!($mbox=imap_open(MAILBOX,USER,PASS)))die('Could not open mailbox'); // Соединение с почтовым сервером
        $num_msg=imap_num_msg($mbox); // Получение кол-ва писем на сервере
        $from = isset($_GET['f'])? intval($_GET['f']): 1;

        // Перебор всех писем
        $k=0;
        for($i=$num_msg;$i>0;$i--) {
            $obj = imap_headerinfo($mbox, $i);
            $arr = $obj->from;
            $personal = $arr[0]->personal; //от кого письмо (подпись)
            $personal=self::icnv($personal, "UTF8");
            $mailbox = $arr[0]->mailbox; //email отправителя, часть до @
            $mailbox=self::icnv($mailbox, "UTF8");
            $host = $arr[0]->host; //email отправителя, часть после @
            $host=self::icnv($host, "UTF8");
            $subject = $obj->Subject; //тема письма
            $subject=self::icnv($subject, "UTF8");

            $structure = imap_fetchstructure($mbox,$i);// Структура письма


            for($p=0;isset($structure->parts) && $p<=count($structure->parts);$p++) {// Перебор частей писем (письмо, состоящее из одной части не содержит аттачей)

                if($structure->parts[$p]->ifdisposition && strtoupper($structure->parts[$p]->disposition)=='ATTACHMENT') {// Если это вложение, ...
                    $buffer = imap_fetchbody($mbox,$i,$p+1,FT_PEEK);//..., то получаем его
                    switch($structure->parts[$p]->encoding) { // Раскодирование
                        case ENC_BASE64: { $buffer=imap_base64($buffer); break; }
                        case ENC_QUOTED_PRINTABLE: { $buffer=quoted_printable_decode($buffer); break; }
                    }
                    $name=$structure->parts[$p]->dparameters[0]->value;
                    pr($structure->parts[$p]->dparameters);
                    $name=self::icnv($name, "UTF8");
                    if(empty($name))$name=$structure->parts[$p]->parameters[0]->value;
                    $attachment[$k]=array(
                        'fileName'=>$name,
                        'fileContent' =>$buffer,
                        'mailPost'=>$subject,
                        'mailFrom'=>$mailbox."@".$host,
                        'personalFrom'=>$personal
                    );
                    $k++;
                }
                else if($structure->parts[$p]->ifsubtype && strtoupper($structure->parts[$p]->subtype)=='PLAIN'){/* Письмо без вложений - не записываеется в массив */ }
            }
            imap_delete($mbox, $i);//помечаем письмо как удаленное
        }
        imap_expunge($mbox);//удаляем помеченные письма
        imap_close($mbox);// Закрытие соединения
        /*
         * все данные из полученных писем занесены в массив, теперь обрабатываем дынные и сохраняем файлы на сервере
         */
        for($i=0;$i<=count($attachment);$i++) {
            //сравнивая значения полей "от кого" и "тема письма" находим соответствие и закачивая файлы переименовываем их для последущей загрузки
            foreach($Sklad as $val)  { // строки совпадают - запоминаем позицию массива
                //обязательными условиями для сравнения - адрес и имя файла  необязательный параметр - тема письма - делаем вторым пропускным контролем
                if(!empty($attachment[$i]['mailFrom']) && !empty($val['mailFrom']) && !empty($val['mailPost'])) {
                    if(substr_count(strtolower($attachment[$i]['mailFrom']),strtolower($val['mailFrom']))>0 || substr_count(strtolower($attachment[$i]['personalFrom']),strtolower($val['mailFrom']))>0) {
                        $str_1 = strtolower($attachment[$i]['mailPost']);
                        $str_2 = strtolower($val['mailPost']);
                        preg_match('/('.str_replace(' ','[ _]+',$str_2).')/',$str_1,$matches_);
                        if(substr_count($str_1,$str_2)>0 or $matches_[1]) {//дополнительный контроль
                            $checkedSklad=$val;
                        }
                    }
                }
            }
            if($checkedSklad) {
                /* если файл не архив */
                if(substr_count(strtolower($attachment[$i]['fileName']),strtolower($checkedSklad['fileName']))>0 && empty($checkedSklad['fileArchiveName'])) {
                    $fh=fopen($_SERVER['DOCUMENT_ROOT']."/Files/".$checkedSklad['fileReName'],'wb');
                    fwrite($fh,$attachment[$i]['fileContent']);
                    fclose($fh);
                }
                /* если файл является архивом */
                if(!empty($checkedSklad['fileArchiveName']) && !empty($attachment[$i]['fileName'])) { //если скаченный файл является архивом, запускаем функцию разархивирования и переименовываем файл
                    $fh=fopen($_SERVER['DOCUMENT_ROOT']."/Files/".$attachment[$i]['fileName'],'wb');
                    fwrite($fh,$attachment[$i]['fileContent']);
                    fclose($fh);
                    self::CheckArhive($_SERVER['DOCUMENT_ROOT']."/Files/");
                    //просматриваем все файлы в директории, если файл точно совпадает с бд или частично, то переименовываем его
                    $dir=opendir($_SERVER['DOCUMENT_ROOT']."/Files/");
                    $i=0;
                    while(($file=readdir($dir))) {
                        if($file==$checkedSklad['fileName'] || substr_count(strtolower($file),strtolower($checkedSklad['fileName']))>0)rename($_SERVER['DOCUMENT_ROOT']."/Files/".$file,$_SERVER['DOCUMENT_ROOT']."/Files/".$checkedSklad['fileReName']);
                    }
                }
            }
        }
    }

    function getSklads() {
        global $base;
        $row = $base->select_("SELECT * FROM `Sklads`;");
        return($row[0]);
    }

    /*
     * функция декодирования
     */
    function icnv ($str,$out_charset) {
        preg_match_all('/=\\?(.+?)\\?(.+?)\\?(.+?)((\\s)|\\?=|$)/', $str, $matches);
        for($i = 0; $i < count($matches[0]); $i++) {
            echo"<br>";
            switch( strtoupper($matches[2][$i]) )
            {
                case 'B':  $matches[3][$i] = imap_base64($matches[3][$i]); break;
                case 'QP': $matches[3][$i] = imap_qprint($matches[3][$i]); break;
                case 'Q':  $matches[3][$i] = quoted_printable_decode($matches[3][$i]); break;
                default: trigger_error('Unknown encoding: ' . $matches[2][$i]);
                return false;
            }
            $matches[3][$i] .= $matches[5][$i];
            if( strtoupper($matches[1][$i]) != strtoupper($out_charset) ) {
                $matches[3][$i] = iconv($matches[1][$i], $out_charset . '//IGNORE', $matches[3][$i]);
            }
        }

        $str = str_replace($matches[0], $matches[3], $str);
        //==============================================================================================================
        $enc = (mb_detect_encoding($str,"UTF-8,CP1251,Base64",true));
        if ($enc=='CP1251') $str = iconv('CP1251',$out_charset,$str);
        if ($enc=='Base64') $str = iconv('Base64','CP1251',$str);
        if ($enc=='ASCII') $str = iconv('ASCII',$out_charset,$str);
        return $str;
    }

    /*
     * функция разархивирует файлы ZIP
     */
    function DeArhiveZIP($FileName,$Folder) {
        $zip = zip_open($FileName);
        $folder = substr(str_replace("\\", "/", __FILE__), 0, strrpos(str_replace("\\", "/", __FILE__), "/"))."/".$Folder;
        $files = 0;
        $folders = 0;
        if ($zip) {
            while ($zip_entry = zip_read($zip)) {
                echo "<br />\n";
                $name = $folder . zip_entry_name($zip_entry);
                $name=self::decode_header($name, 'windows-1251');
                if($name[strlen($name) - 1] == '/') {
                    mkdir($name, 0755);
                    $folders++;
                } elseif (zip_entry_open($zip, $zip_entry, "r")) {
                    $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                    $file = fopen($name, "w");
                    if ($file) {
                        fwrite($file, $buf);
                        fclose($file);
                        $files++;
                    }
                    zip_entry_close($zip_entry);
                }
            }
            zip_close($zip);
        }
    }

    /*
     * функция разархивирует файлы RAR
     */
    function DeArhiveRAR($FileName,$Folder) {
        $archive = RarArchive::open($Folder."".$FileName);
        $list = $archive ->getEntries();
        foreach ($list as $entity) {
            $entity->extract($Folder,$Folder."".$entity->getName());
        }
    }

    /*
     * функция находит файлы-архивы и запускает функцию разархивирования - надо указать путь к папке, где искать архивы
     */
    function CheckArhive($Folder) {
        global $attachment;
        $dir = opendir($Folder);// Открываем директорию
        while(($file = readdir($dir))) { // В цикле считываем её содержимое
            $file=(string)$file;
            if($file!="." && $file!="..") {
                if(is_resource(zip_open($Folder."".$file))){
                    self::DeArhiveZIP($Folder."".$file,$Folder);
                }elseif(substr_count(strtolower($file),"rar")>0) {
                    self::DeArhiveRAR($file,$Folder);
                }
            }
        }
        closedir($dir); //закрываем директорию
    }

}



?>