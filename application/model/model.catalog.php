<?php


	class mCatalog {

    	function showTypes () {
            global $base,$smarty;
            $items = $base->select_("SELECT * FROM `Catalog` WHERE `SubId`=0 ORDER BY `Name`;");
            foreach($items as $i=>$value) {
            	$row1 = $base->select_("SELECT * FROM `Catalog` WHERE `SubId`='".$value[Id]."' ORDER BY `Name`;");
            	foreach($row1 as $val) {
            		$items[$i]['submenu'][]=$val;
            	}
            }
            $smarty->assign('itm', $items);
            $smarty->assign('items', $items);
            core::addTemplate('Catalog',2);
    	}

    	function addItem () {
            global $base,$smarty;
            $row = $base->select_("SELECT * FROM `Catalog` WHERE `Name` LIKE '".$_POST['nameItem']."';");
            if(!$row)$row = $base->do_("INSERT INTO `Catalog` (`Id`,`SubId`,`Name`,`Image`) VALUES('','0','".$_POST['nameItem']."','');");
            self::showTypes();
    	}

    	function addSubItem () {
            global $base,$smarty;
            $row = $base->select_("SELECT * FROM `Catalog` WHERE `Name` LIKE '".$_POST['nameSubItem']."' AND `SubId`='".$_POST['IdItem']."';");
            if(!$row)$row = $base->do_("INSERT INTO `Catalog` (`Id`,`SubId`,`Name`,`Image`) VALUES('','".$_POST['idItem']."','".$_POST['nameSubItem']."','');");
            self::showTypes();
    	}

        function deleteItem ($Id) {
            global $base,$smarty;
            $row = $base->select_("SELECT * FROM `Catalog` WHERE `SubId`='".$Id."';");
            $row1 = $base->select_("SELECT * FROM `GoodsList` WHERE `IdCatalog`='".$Id."';");
            if(!$row && !$row1)$res = $base->do_("DELETE FROM `Catalog` WHERE `Id`='".$Id."';");
            self::showTypes();
        }

        function getGoodsList ($id,$columnSort) {
            global $base;
            $res = $base->select_("
            SELECT * FROM
            `GoodsList`, `Proizvoditel`
            WHERE `goodIdCatalog`='$id' and `GoodsList`.`goodIdProizv`=`Proizvoditel`.`prId`
            ORDER BY '$columnSort';
            ");
            return $res;
        }

        function getGoodProizvNames () {
            global $base;
            $prNames = $base->select_("SELECT * FROM `Proizvoditel` ORDER BY `prName`;");
            return $prNames;
        }

        function redactItem($Id) {
            global $base;
            /* && !file_exists("/siteImages/".$_FILES["fileName"]["name"])*/
            //if($_FILES['filename']['name']) {
                self::loadItemFoto($Id,$_SERVER['DOCUMENT_ROOT']."/Images/siteImages/"); /*если указан файл для загрузки, загружаем его*/
            //    $res = $base->do_("UPDATE `Catalog` SET `Name`='".$_POST['itemName']."r' WHERE `Id`='$Id';");
            //}
            $res = $base->do_("UPDATE `Catalog` SET `Name`='".$_POST['itemName']."' WHERE `Id`='$Id';");
            return $res;
        }

        function explodeGoodImages($goods) {
            global $base;
            foreach($goods as $i=>$value) {
                $images[$value[goodId]][]=explode("/",$value[goodFoto]);
            }
            return $images;
        }

        /* загрузка фотографий для групп товаров
         * filename - имя загружаемого файла фотографии
         * path - путь, куда сохранять файлы*/
        function loadItemFoto($id,$path) {
            global $base;
            if(is_uploaded_file($_FILES["filename"]["tmp_name"])) { /*проверяет загружен ли файл на сервер*/
                move_uploaded_file($_FILES["filename"]["tmp_name"], $path.$_FILES["filename"]["name"]);
            }
            /*проверяем, если уже была загружена фотграфия-файл, то удаляем старый файл, а потом загружаем новый*/
            $row = $base->select_("SELECT `Catalog`.`Image` FROM `Catalog` WHERE `Id`='$id';");
            if($row) {
                pr("удаляем старый файл");
                self::deleteItemFoto($id,$path);
            }/**/
            //записываем имя файла в БД
            //проверяем не загружен ли этот файл
            $res = $base->do_("UPDATE `Catalog` SET `Image`='".$_FILES["filename"]["name"]."' WHERE `Id`='$id';");
            //self::resizeImage($path.$_FILES["fileName"]["name"], $path.$_FILES["fileName"]["name"], $w = 200, $q = 90);
        }

        /* загрузка фотографий для товаров
         * filename - имя загружаемого файла фотографии
         * path - путь, куда сохранять файлы*/
        function loadGoodFoto($id,$path,$numFoto) {
            global $base;

            if(is_uploaded_file($_FILES["filename"]["tmp_name"])) { /*проверяет загружен ли файл на сервер*/
                pr("загружаю");
                move_uploaded_file($_FILES["filename"]["tmp_name"], $path.$_FILES["filename"]["name"]);/*загружаем*/
                /*вытаскиваем из базы наименования всех картинок, принадлежащих данной позиции товара и меняем имя нужной*/
                $row = $base->select_("SELECT `GoodsList`.`goodFoto` FROM `GoodsList` WHERE `goodId`='$id';");
                $images=explode("/", $row[0]['goodFoto']);
                for($i=1;$i<=4;$i++) if($numFoto==$i){
                    $newImagesName.="/".$_FILES["filename"]["name"];
                }else{
                    $newImagesName.="/".$images[$i];
                }
                $newImagesName.="/";
                //записываем имена файлов в БД
                $row = $base->do_("UPDATE `GoodsList` SET `goodFoto`='$newImagesName' WHERE `goodId`='$id';");
            }else{
                pr("Файл с таким именем уже существует");
                return false;
            }

            self::watermark($path."".$_FILES["filename"]["name"],$path."/WaterZnak.png");
            self::resizeImage($path."".$_FILES["filename"]["name"], $path."smallGoodImages/".$_FILES["filename"]["name"], $w = 200, $q = 150);
        }

        function deleteItemFoto($id,$path) {
            global $base;
            $res = $base->select_("SELECT * FROM `Catalog` WHERE `Id`='".$id."';");
            if(file_exists($path.$res[0]['Image'])){
                pr("файл существует - удаляем его");
                unlink($path.$res[0]['Image']);
            } /*удаляем файл*/
            $row = $base->do_("UPDATE `Catalog` SET `Image`=NULL;"); /*удаляем ссылку на файл из БД*/
        }

        function deleteGoodFoto($id,$path,$numFoto) {
            global $base;
            $res = $base->select_("SELECT * FROM `GoodsList` WHERE `Id`='".$id."';");
            if(file_exists($path.$res[0]['goodFoto'])){
                pr("файл существует - удаляем его");
                unlink($path.$res[0]['goodFoto']);
            } /*удаляем файл*/
            $row = $base->do_("UPDATE `Catalog` SET `Image`=NULL;"); /*удаляем ссылку на файл из БД*/
        }

        function resizeImage($file, $out, $w = 200, $q = 90) { /*изменяет размеры фотографии*/
            pr($file);
            if(empty($file) | empty($out)) return false;
            $src = imagecreatefromjpeg($file);
            $w_src = imagesx($src);
            $h_src = imagesy($src);
            $ratio = $w_src/$w;
            $w_dest = round($w_src/$ratio);
            $h_dest = round($h_src/$ratio);
            $dest = imagecreatetruecolor($w_dest, $h_dest);
            imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
            imagejpeg($dest, $out, $q);
            imagedestroy($dest);
            imagedestroy($src);
            return true;
        }

        function watermark($file, $watermark) { /*накладывает на фотографию водяной знак(наложение фотографии на фотографию)*/
            if(empty($file) | empty($watermark)) return false;
            $wh = getimagesize($watermark);
            $fh = getimagesize($file);
            $rwatermark = imagecreatefrompng($watermark); //Иногда может понадобиться наложить прозрачный png, тогда заменяем функцию на imagecreatefrompng
            $rfile = imagecreatefromjpeg($file);
            imagecopy($rfile, $rwatermark, $fh[0] - $wh[0], $fh[1] - $wh[1], 0, 0, $wh[0], $wh[1]);
            imagejpeg($rfile, $file, '100');
            imagedestroy($rwatermark);
            imagedestroy($rfile);
            return true;
        }

        function redactGood($id) {
            global $base;
            //`goodIdModel`='',
            $base->do_("UPDATE `GoodsList` SET
            `goodArt`='".$_POST['Art']."',
            `goodKod`='".$_POST['Kod']."',
            `goodKod2`='".$_POST['Kod2']."',
            `goodName`='".$_POST['Name']."',
            `goodPrimenimost`='".$_POST['Primenimost']."',
            `goodIdProizv`='".$_POST['idProizv']."',
            `goodTypeGood`='".$_POST['TypeGood']."',
            `goodSkidka`='".$_POST['Skidka']."',
            `goodMinKol`='".$_POST['MinKol']."',
            `goodMaxKol`='".$_POST['MaxKol']."',
            `goodShowInPrice`='".$_POST['ShowInPrice']."',
            `goodShowInShop`='".$_POST['ShowInShop']."',
            `goodPriceValZak`='".$_POST['PriceValZak']."',
            `goodValuta`='".$_POST['Valuta']."',
            `goodPriceVal`='".$_POST['PriceVal']."',
            `goodPrice`='".$_POST['Price']."',
            `goodStopPrice`='".$_POST['StopPrice']."',
            `goodNac1`='".$_POST['goodNac1']."',
            `goodNac2`='".$_POST['goodNac2']."',
            `goodNac3`='".$_POST['goodNac3']."',
            `goodNac4`='".$_POST['goodNac4']."',
            `goodNac5`='".$_POST['goodNac5']."',
            `goodNac6`='".$_POST['goodNac6']."',
            `goodNac7`='".$_POST['goodNac7']."',
            `goodNac8`='".$_POST['goodNac8']."',
            `goodNac9`='".$_POST['goodNac9']."',
            `goodNac10`='".$_POST['goodNac10']."'
            WHERE `goodId`='".$id."';");
        }

        function getCarsList($array) {
            global $base;
            $res = $base->select_("SELECT * FROM `Cars` WHERE `carsSubId`=0 ORDER BY `carsName`;");
            foreach($res as $val) {
                $carsMarka[] = array("Id"=>$val['carsId'],"SubId"=>$val['carsSubId'],"Name"=>$val['carsName'],"Description"=>$val['carsDescription']);
                $row = $base->select_("SELECT * FROM `Cars` WHERE `carsSubId`='".$val['carsId']."' ORDER BY `carsName`;");
                foreach($row as $val2) {
                    $carsModel[$val['carsName']][] = array("Id"=>$val2['carsId'],"SubId"=>$val2['carsSubId'],"Name"=>$val2['carsName'],"Description"=>$val2['carsDescription']);
                }
            }
            if($array=='Marka')return $carsMarka;
            if($array=='Model')return $carsModel;
        }

        function redactCars($id) {
            global $base;
            $res = $base->do_("UPDATE `Cars` SET `carsName`='".$_POST['modelName']."', `carsDescription`='".$_POST['modelYear']."' WHERE `carsId`='".$id."'");
        }

        function addCar($type) {
            global $base;
            if($type=='marka')$res = $base->do_("INSERT INTO `Cars` (`carsId`,`carsSubId`,`carsName`,`carsDescription`) VALUES ('','0','".$_POST['Name']."','')");
            if($type=='model')$res = $base->do_("INSERT INTO `Cars` (`carsId`,`carsSubId`,`carsName`,`carsDescription`) VALUES ('','".$_POST['SubId']."','".$_POST['Name']."','".$_POST['Year']."')");
        }

        /*
         * эта функция обрабатывает строку, в которой хранятся id автомобилей, для которых подходит та или иная запчасть
         * например, строка "/1/2/3/4/5/6/"
         * функция преобразует эту строку в массив array[1,2,3,4,5,6]
         * $id - параметр, который передаем в эту функцию - это id раздела
         * т.е. создается двумерный массив
         */
        function explodeCars($id) {
            global $base;
            $row = $base->select_("SELECT `goodId`,`goodIdModel` FROM `GoodsList` WHERE `goodIdCatalog`='".$id."';");
            foreach($row as $val) {
                $checkedCars[$val['goodId']] = explode("/",$val['goodIdModel']);
            }
            //pr($checkedCars);
            return($checkedCars);
        }

        function RedactCheckedCar($id) {
            global $base,$checkbox;
            if($checkbox)$str.="/";
            foreach($checkbox as $val) {
                if($val)$str.=$val."/";
            }
            $sql = $base->do_("UPDATE `GoodsList` SET `goodIdModel`='".$str."';");
        }

	}


?>