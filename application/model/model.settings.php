<?php

    class mSettings {

        function redactSklads($id) {
            global $base,$smarty;

            if($id=="new") {
                $str = "INSERT INTO `Sklads`
                (`Id` ,
                `updateDate` ,
                `sklName` ,
                `sklProName` ,
                `sklInfo` ,
                `dostSrokMin` ,
                `dostSrokMax` ,
                `priceSkidka` ,
                `priceVal` ,
                `prePay` ,
                `daysTimeOut` ,
                `AutoSender` ,
                `mailTextZakaz` ,
                `mailForSend` ,
                `fileGetType` ,
                `fileArchiveName` ,
                `fileName` ,
                `fileReName` ,
                `fileReadLine` ,
                `mailFrom` ,
                `mailPost` ,
                `loadDays` ,
                `loadLink` ,
                `dostPriceZak` ,
                `dostValuta` ,
                `dostPrice` ,
                `sklType` ,
                `tableName` ,
                `tableNamePrice` ,
                `fileParsingInfo` ,
                `Block` ,
                `Nac1` ,
                `Nac2` ,
                `Nac3` ,
                `Nac4` ,
                `Nac5` ,
                `Nac6` ,
                `Nac7` ,
                `Nac8` ,
                `Nac9` ,
                `Nac10`)
                VALUES ('', '', '".$_POST['newSkladName']."', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');";
            }else{
                $str = "UPDATE `Sklads` SET
                `sklName`='".$_POST['sklName']."',
                `sklProName`='".$_POST['sklProName']."',
                `sklInfo`='".$_POST['sklInfo']."',
                `dostSrokMin`='".$_POST['dostSrokMin']."',
                `dostSrokMax`='".$_POST['dostSrokMax']."',
                `priceSkidka`='".$_POST['priceSkidka']."',
                `priceVal`='".$_POST['priceVal']."',
                `prePay`='".$_POST['prePay']."',
                `daysTimeOut`='".$_POST['daysTimeOut']."',
                `AutoSender`='".$_POST['AutoSender']."',
                `mailTextZakaz`='".$_POST['mailTextZakaz']."',
                `mailForSend`='".$_POST['mailForSend']."',
                `fileGetType`='".$_POST['fileGetType']."',
                `fileArchiveName`='".$_POST['fileArchiveName']."',
                `fileName`='".$_POST['fileName']."',
                `fileReName`='".$_POST['fileReName']."',
                `fileReadLine`='".$_POST['fileReadLine']."',
                `mailFrom`='".$_POST['mailFrom']."',
                `mailPost`='".$_POST['mailPost']."',
                `loadDays`='".$_POST['loadDays']."',
                `loadLink`='".$_POST['loadLink']."',
                `dostPriceZak`='".$_POST['dostPriceZak']."',
                `dostValuta`='".$_POST['dostValuta']."',
                `dostPrice`='".$_POST['dostPrice']."',
                `sklType`='".$_POST['sklType']."',
                `tableName`='".$_POST['tableName']."',
                `tableNamePrice`='".$_POST['tableNamePrice']."',
                `fileParsingInfo`='".$_POST['fileParsingIfo']."',
                `Block`='".$_POST['Block']."',
                `Nac1`='".$_POST['Nac1']."',
                `Nac2`='".$_POST['Nac2']."',
                `Nac3`='".$_POST['Nac3']."',
                `Nac4`='".$_POST['Nac4']."',
                `Nac5`='".$_POST['Nac5']."',
                `Nac6`='".$_POST['Nac6']."',
                `Nac7`='".$_POST['Nac7']."',
                `Nac8`='".$_POST['Nac8']."',
                `Nac9`='".$_POST['Nac9']."',
                `Nac10`='".$_POST['Nac10']."'
                WHERE `Id`='".$id."'
                ";
            }
            if($str) {
                //выполняем сохранение в БД изменений данных или вставляем в БД новыю запись
                $sql = $base->do_($str);

                //если надо, создаем таблицу
                if(!empty($_POST["tableName"]) && empty($_POST["preTableName"]) && empty($_POST["table"])) {
                    //создаем таблицу
                    self::createTable($_POST["tableName"]);
                }
                if($_POST["preTableName"]!=$_POST["tableName"] && !empty($_POST["preTableName"]) && empty($_POST["table"])) {
                    //переименовываем таблицу
                    self::reNameTable($_POST["preTableName"],$_POST["tableName"]);
                }
                if($_POST["preTableName"]!=$_POST["tableName"] && !empty($_POST["preTableName"]) && empty($_POST["tableName"])) {
                    //удаляем таблицу
                    self::deleteTable($_POST["preTableName"]);
                }
            }

            $sklad = $base->select_("SELECT * FROM `Sklads` ORDER BY `sklName`;");
            $k=0;
            foreach($sklad as $i=>$value) {
                if(substr_count($value["updateDate"],date("d.m.y"))>0) {
                    $sklad[$k]["updateDate"]="Сегодня [".$value["updateDate"]."]";
                }
                if(!$value["updateDate"])$sklad[$k]["updateDate"]="никогда";
                $k++;
            }
            $smarty->assign('sklad', $sklad);
            core::addTemplate("settingsSklads",2);
        }

        function deleteSklad($id) {
            global $base;
            $row = $base->select_("SELECT * FROM `Sklads` WHERE `Id`='".$id."';");
            if($row[0]["sklType"]==3) {
                $res = $base->select_("SELECT `Id`,`tableNamePrice` FROM `Sklads` WHERE `sklType`='2';");
                foreach($res as $val) {
                    if($val["tableNamePrice"]==$row[0]["tableName"]) {
                        $dd = $base->do_("UPDATE `Sklads` SET `tableNamePrice`=NULL WHERE `Id`='".$val["Id"]."';");
                    }
                }
            }

            self::deleteTable($row[0]["tableName"]);
            $del = $base->do_("DELETE FROM `Sklads` WHERE `Id`='".$id."';");
        }

        function createTable($name) {
            global $base;
            $str="CREATE TABLE `{$name}`
            (`Art` INT NOT NULL ,
            `Kod` TEXT NOT NULL ,
            `Name` TEXT NOT NULL ,
            `Proizv` VARCHAR( 64 ) NOT NULL ,
            `Price` FLOAT NOT NULL ,
            `Ost` VARCHAR( 32 ) NOT NULL
            ) ENGINE = MYISAM";
            $sql1 = $base->do_($str);
            $sql2 = $base->do_("ALTER TABLE `{$name}` ADD INDEX ( `Art` );");
            //pr("создали таблицу");
        }

        function deleteTable($name) {
            global $base;
            $sql = $base->do_("DROP TABLE `{$name}`;");
            //pr("удалили таблицу");
        }

        function reNameTable($name,$reName) {
            global $base;
            $sql = $base->do_("ALTER TABLE ".$name." RENAME ".$reName.";");
            //pr("переименовали таблицу");
        }

        function redactProizv() {
            global $base,$smarty;
            $proizv = $base->select_("SELECT * FROM `Proizvoditel` ORDER BY `prName`");
            $smarty->assign('proizv', $proizv);
            core::addTemplate("settingsProizv",2);
        }

    }

?>