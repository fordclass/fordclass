<?php
global $templates;
	class core{
 		public static function useController ($name) {
 			include ("application/controller/controller.{$name}.php");
 		}


 		public static function useModel ($name) {
 			$class = "m".ucfirst($name);
 			if (class_exists($class)) return;
 			include ("application/model/model.{$name}.php");
 		}


 		public static function useView ($name) {
 			//global $views;
 			include ("application/view/{$name}.php");
 		}


		public static function router () {
			global $smarty;
			//pr (__FILE__.__FUNCTION__);
			//pr ($_SERVER['REQUEST_URI']);

			$purl = parse_url ($_SERVER['REQUEST_URI']);
			$req = explode("/",$purl['path']);
			//pr ($req);

			if (!$req[1]) $req[1] = 'index';
			self::useController ($req[1]);
			$class = "c".ucfirst($req[1]);

			if (!$req[2]) $req[2] = 'index';

			if (!method_exists($class,$req[2])) die ('page 404');
			$classObj = new $class;

			$classObj->$req[2]($req[3],$req[4],$req[5],$req[6]);

			$smarty->assign("MenuItem", $req[1]);
			$smarty->assign("SubMenuItem", $req[2]);
		}


		public static function assignView ($var,$value) {
			global $view;
			$view[$var] = $value;
		}


		public static function addTemplate ($name,$part) {
			global $templates;
			$templates[$part][] = $name;
		}


		public static function showTemplates () {
			global $templates;
			pr ($templates);
		}

	}

?>